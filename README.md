CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation and usage

INTRODUCTION
------------

Build Drupal systems using natural language with the help of AI.

At the moment has API functions to gather and preprocess source code.

For more information visit project page https://drupal.org/project/aidev

REQUIREMENTS
------------

None

INSTALLATION AND USAGE
----------------------

- Follow the standard Drupal installation instructions:
  https://www.drupal.org/docs/extending-drupal/installing-modules

- Module provides API methods to use within your custom code:

```php
// Get service
$service = \Drupal::service('aidev.source_code');

// Define files to preprocess
$modifiers = [

  // mention every file that exist
  ["status" => 1, "action" => "mention", "paths" => ["*"]],

  // completely exclude README.md file
  ["status" => 1, "action" => "exclude", "paths" => ["README.md"]],

  // include full contents of some files
  ["status" => 1, "action" => "include", "paths" => ["aidev.info.yml", "src/Plugin"]],

  // this rule is disabled and does nothing
  ["status" => 0, "action" => "exclude", "paths" => ["ExcludeFilePlugin.php"]],
];

// Now you can either 1) gather source code from particular directory like this
$path = 'modules/aidev/src';
$source_code = $service->gatherDirectorySource($path, $modifiers);

// Or 2) gather source code of particular project like this
$project_type = 'module';
$project_name = 'aidev';
$source_code = $service->gatherProjectSource($project_type, $project_name, $modifiers);
```

The `$source_code` variable will now contain an array of the gathered source code and related statistics
