<?php

namespace Drupal\aidev\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a SourceModifier item annotation object.
 *
 * @see \Drupal\aidev\SourceModifierPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class SourceModifier extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A brief description of the plugin.
   *
   * This will be shown when adding or configuring this plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}
