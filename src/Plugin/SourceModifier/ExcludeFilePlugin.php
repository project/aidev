<?php

namespace Drupal\aidev\Plugin\SourceModifier;

use Drupal\aidev\Plugin\SourceModifierPluginBase;

/**
 * @SourceModifier(
 *   id = "exclude",
 *   label = @Translation("Exclude file"),
 *   description = @Translation("Returns empty string")
 * )
 */
class ExcludeFilePlugin extends SourceModifierPluginBase {

  /**
   * {@inheritdoc}
   */
  public function applyModifier(string $source): string {
    // empty string is interpreted as file is excluded
    return '';
  }

}