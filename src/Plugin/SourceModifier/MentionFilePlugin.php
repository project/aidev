<?php

namespace Drupal\aidev\Plugin\SourceModifier;

use Drupal\aidev\Plugin\SourceModifierPluginBase;

/**
 * @SourceModifier(
 *   id = "mention",
 *   label = @Translation("Mention"),
 *   description = @Translation("Omits source code.")
 * )
 */
class MentionFilePlugin extends SourceModifierPluginBase {

  /**
   * {@inheritdoc}
   */
  public function applyModifier(string $source): string {
    return '[file source omitted]';
  }

}
