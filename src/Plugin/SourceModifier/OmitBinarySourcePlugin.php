<?php

namespace Drupal\aidev\Plugin\SourceModifier;

use Drupal\aidev\Plugin\SourceModifierPluginBase;

/**
 * @SourceModifier(
 *   id = "omit_binary",
 *   label = @Translation("Omit binary"),
 *   description = @Translation("Omits binary.")
 * )
 */
class OmitBinarySourcePlugin extends SourceModifierPluginBase {

  /**
   * {@inheritdoc}
   */
  public function applyModifier(string $source): string {
    return '[binary file omitted]';
  }

}