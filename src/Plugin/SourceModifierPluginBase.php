<?php

namespace Drupal\aidev\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Source modifier plugins.
 */
abstract class SourceModifierPluginBase extends PluginBase implements SourceModifierPluginInterface {

  /**
   * {@inheritdoc}
   */
  abstract public function applyModifier(string $source): string;

}