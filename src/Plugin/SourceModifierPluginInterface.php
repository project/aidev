<?php
namespace Drupal\aidev\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Source modifier plugins.
 */
interface SourceModifierPluginInterface extends PluginInspectionInterface {

  /**
   * Apply the source modifier.
   *
   * @param string $source
   *   The source code.
   *
   * @return string
   *   The modified source code.
   */
  public function applyModifier(string $source): string;

}
