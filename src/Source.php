<?php

namespace Drupal\aidev;

use Drupal\aidev\Exception\AIDevException;
use Drupal\aidev\SourceModifierPluginManager;
use Drupal\Core\Extension\Exception\UnknownExtensionException;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ThemeExtensionList;

/**
 * Provides a class to gather the source code from a specific module or theme.
 * This class is responsible for the handling of source code including gathering, 
 * preprocessing, and calculating statistics about the code.
 * 
 * @see README.md for usage documentation.
 */
class Source implements SourceInterface {

  /**
   * The module extension list service.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * The theme extension list service.
   *
   * @var \Drupal\Core\Extension\ThemeExtensionList
   */
  protected $themeExtensionList;

  /**
   * The source modifier plugin manager.
   *
   * @var \Drupal\aidev\SourceModifierPluginManager
   */
  protected $sourceModifierManager;

  /**
   * An array to hold the plugin instances.
   *
   * @var array
   */
  protected $sourceModifiers = [];

  /**
   * Constructs a new Source object.
   *
   * @param \Drupal\Core\Extension\ModuleExtensionList $module_extension_list
   *   The module extension list service.
   * @param \Drupal\Core\Extension\ThemeExtensionList $theme_extension_list
   *   The theme extension list service.
   * @param \Drupal\aidev\SourceModifierPluginManager $source_modifiers
   *   The source modifier plugin manager.
   */
  public function __construct(ModuleExtensionList $module_extension_list, ThemeExtensionList $theme_extension_list, SourceModifierPluginManager $source_modifier_manager) {
    $this->moduleExtensionList = $module_extension_list;
    $this->themeExtensionList  = $theme_extension_list;
    $this->sourceModifierManager = $source_modifier_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function gatherProjectSource(string $project_type, string $project_name, array $modifiers = []): array {
    $directory = $this->getProjectPath($project_type, $project_name);
    $source_code = $this->gatherDirectorySource($directory, $modifiers);
    return $source_code;
  }

  /**
   * {@inheritdoc}
   */
  public function gatherDirectorySource(string $directory, array $modifiers = []): array {

    if (!is_dir($directory)) {
      throw new AIDevException("The directory {$directory} for gathering source code does not exist.");
    }

    // Create a RecursiveDirectoryIterator and a RecursiveIteratorIterator.
    $directory_iterator = new \RecursiveDirectoryIterator($directory, \RecursiveDirectoryIterator::SKIP_DOTS);
    $iterator = new \RecursiveIteratorIterator($directory_iterator);

    $source_code = [];

    // Iterate through all files and subdirectories in the module.
    foreach ($iterator as $file) {

      // If the current file is a regular file, read its contents and store them in the associative array.
      if ($file->isFile()) {

        $modifier_action = '';
        $pathname = $file->getPathname();

        $file_contents = file_get_contents($pathname);

        // remove binary files early
        if ($this->isBinary($file_contents)) {
          $modifier_action = 'omit_binary';
          $this->applyModifier($file_contents, $modifier_action);
        }

        $relative_file_path = str_replace($directory . '/', '', $pathname);

        $source_code[$relative_file_path] = [
          'relative_path' => $relative_file_path,
          'modifier' => $modifier_action,
          'source' => $file_contents,
          //'file_path' => $file_path
        ];
      }
    }

    $this->markModifiers($source_code, $modifiers);
    $this->applyModifiers($source_code);
    $this->calcStatistics($source_code);

    return $source_code;
  }

  /**
   * {@inheritdoc}
   */
  public function getProjectPath(string $project_type, string $project_name): string {

    try {
      switch ($project_type) {
        case 'theme':
          $path = $this->themeExtensionList->getPath($project_name);
          break;
        case 'module':
          $path = $this->moduleExtensionList->getPath($project_name);
          break;
        default:
          $message = "Invalid project type \"$project_type\" provided. ";
          $message .= "Project type should be either 'module' or 'theme'";
          throw new AIDevException($message);
      }
    }
    catch (UnknownExtensionException $exception) {
      $message = ucfirst($project_type)." with a name \"$project_name\" not found.";
      throw new AIDevException($message);
    }

    return $path;
  }

  /**
   * Marks the modifiers in the source code.
   */
  protected function markModifiers(array &$source_code, array $modifiers): void {

    foreach ($source_code as $path => $data) {
      foreach ($modifiers as $key => $val) {

        if (empty($val['status'])) continue;

        if ($this->matchesPatterns($path, $val['paths'])) {
          $source_code[$path]['modifier'] = $val['action'];
        }
      }
    }
  }

  /**
   * Applies the modifiers to the source code.
   */
  protected function applyModifiers(array &$source_code): void {
    foreach ($source_code as $path => $data) {
      if (!empty($data['modifier'])) {
        $this->applyModifier($source_code[$path]['source'], $data['modifier']);
      }
    }
  }

  /**
   * Applies a modifier to the source code.
   */
  protected function applyModifier(string &$source_code, string $modifier_name): void {
    if (!isset($this->sourceModifiers[$modifier_name])) {
      if ($this->sourceModifierManager->hasDefinition($modifier_name)) {
        $this->sourceModifiers[$modifier_name] = $this->sourceModifierManager->createInstance($modifier_name);
      }
    }

    if (!empty($this->sourceModifiers[$modifier_name])) {
      $source_code = $this->sourceModifiers[$modifier_name]->applyModifier($source_code);
    }

    elseif (!in_array($modifier_name, ['include'])) {
      throw new AIDevException("Modifier with the name $modifier_name does not exist.");
    }
  }

  /**
   * Calculates the statistics of the source code.
   */
  protected function calcStatistics(array &$source_code): void {
    foreach ($source_code as $path => $data) {
      $word_count = str_word_count($data['source']);

      $source_code[$path]['symbol_count'] = strlen($data['source']);
      $source_code[$path]['line_count'] = count(preg_split('/\r\n|\r|\n/', $data['source']));
      $source_code[$path]['word_count'] = $word_count;
      $source_code[$path]['approx_token_count'] = round($word_count * 2.5);
    }
  }

  /**
   * Checks if the given variable is a binary string.
   */
  protected function isBinary(mixed $variable): bool {

    // Non string variables are not binary
    if (!is_string($variable)) {
      return FALSE;
    }
  
    // Null bytes is an indicator of binary content
    if (strpos($variable, "\0") !== FALSE) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Checks if the file or directory matches the provided patterns.
   */
  protected function matchesPatterns(string $file_path, array $patterns): bool {

    foreach ($patterns as $pattern) {

      // if contains * or ? symbols
      if (strpos($pattern, '*') !== FALSE || strpos($pattern, '?') !== FALSE) {

        $pattern2 = '~(^|/)' . preg_quote($pattern, '~') . '(/|$)~';
        $pattern2 = str_replace(['\*', '\?'], ['.*', '.'], $pattern2);

        if (preg_match($pattern2, $file_path)) {
          return TRUE;
        }
      }

      // Check if $exclusion appears in the file path and is a separate path part
      // (i.e., right after a path separator, before a separator, or both)
      elseif (preg_match('~(^|/)' . preg_quote($pattern, '~') . '(/|$)~', $file_path)) {
        return TRUE;
      }
    }

    return FALSE;
  }

}