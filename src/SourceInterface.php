<?php

namespace Drupal\aidev;

/**
 * Interface for the Source class.
 */
interface SourceInterface {

  /**
   * Gathers the source code from the project.
   *
   * @param string $project_type
   *   The project type (module or theme).
   * @param string $project_name
   *   The project name.
   * @param array $modifiers
   *   The list of files and directories to preprocess.
   *
   * @return array
   *   An associative array containing the source code.
   */
  public function gatherProjectSource(string $project_type, string $project_name, array $modifiers = []): array;

  /**
   * Gathers the source code from a particular directory.
   *
   * @param string $directory
   *   The directory path.
   * @param array $modifiers
   *   The list of files and directories to preprocess.
   *
   * @return array
   *   An associative array containing the source code.
   */
  public function gatherDirectorySource(string $directory, array $modifiers = []): array;

  /**
   * Returns the path to the specified project (module or theme).
   *
   * @param string $project_type
   *   The project type (module or theme).
   * @param string $project_name
   *   The project name.
   *
   * @return string
   *   The absolute path to the project
   */
  public function getProjectPath(string $project_type, string $project_name): string;

}