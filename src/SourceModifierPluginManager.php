<?php
namespace Drupal\aidev;

use Drupal\aidev\Annotation\SourceModifier;
use Drupal\aidev\Plugin\SourceModifierPluginInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Source modifier plugin manager.
 */
class SourceModifierPluginManager extends DefaultPluginManager {

  /**
   * Constructs a new SourceModifierPluginManager object.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/SourceModifier',
      $namespaces,
      $module_handler,
      SourceModifierPluginInterface::class,
      SourceModifier::class
    );

    $this->alterInfo('aidev_source_modifier_info');
    $this->setCacheBackend($cache_backend, 'aidev_source_modifier_plugins');
  }

}